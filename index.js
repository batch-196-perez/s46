console.log("Hello!");

//mock database

let posts = [];

//count variable that will serve as the post id
let count = 1;

//Add post data
document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	//prevent the page from loading, hinders the default behavior of the event
	e.preventDefault();

	posts.push({
		id: count,
		title : document.querySelector("#txt-title").value,
		body:document.querySelector("#txt-body").value
	});

	count++;
	showPosts(posts);

	alert("Movie Successfully Added!");
	//console.log(posts);
});



//Show 
const showPosts = (posts) => {
	let postEntries = '';
	console.log(posts);

	posts.forEach((post) => {
		console.log(post);
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>

				<button onclick="editPost('${post.id}')">Edit</button>


				<button onclick="deletePost('${post.id}')">Delete</button>

			</div>
		`;

	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}


//Edit Post

const editPost = (id) => {

	
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;	

};

// Update Post

document.querySelector("#form-edit-post").addEventListener('submit', (e) =>{

	e.preventDefault();

	for(let i = 0; i < posts.length; i++){
		//(posts[i].id.toString()  is an int, thats why toString() to convert it into string ==== document.querySelector('#txt-edit-id').value) -- is a string
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){

			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert('Movie post Successfully updated');

			break;
		}
	}
})



// Activity Solution


const deletePost =(id) =>{

 	for(let i = 0; i < posts.length; i++){
 		//console.log(i);
 		if(posts[i].id.toString() === id ){
 			//console.log(posts[i]);
 			posts.splice(i,1);
 			break;
 		}
	}
	showPosts(posts);
}


// const deletePost = (id) => {
// 	let p = posts;
// 	console.log(p);

// 	p.forEach((item,index,arr) =>{
// 		if(item === p.id){
// 			arr.splice(index,1);

// 		}

// 	})
// 	console.log()
// }